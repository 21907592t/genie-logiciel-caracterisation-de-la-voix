package fr.polytech.larynxapp.controller.file;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.io.TarsosDSPAudioFormat;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.pitch.PitchDetectionHandler;
import be.tarsos.dsp.pitch.PitchDetectionResult;
import be.tarsos.dsp.pitch.PitchProcessor;
import be.tarsos.dsp.pitch.Yin;
import fr.polytech.larynxapp.R;

public class FileFragment extends Fragment {

    private static final int PICKFILE_RESULT_CODE = 1;
    private String filePath;

        public static AudioDispatcher dispatcher;
        public float pitchInHz;
        public int millSecond;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_file, container, false);      //Sets the view for the fragment
        Button bt1 = (Button) root.findViewById(R.id.getFileButton);
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Ceci est un test", Toast.LENGTH_SHORT).show();

                Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
                chooseFile.addCategory(Intent.CATEGORY_OPENABLE);
                chooseFile.setType("audio/wav");
                startActivityForResult(
                        Intent.createChooser(chooseFile, "Choose a file"), PICKFILE_RESULT_CODE);
            }
        });

        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case PICKFILE_RESULT_CODE: {
                if (resultCode == Activity.RESULT_OK &&
                        data!=null && data.getData()!=null)
                {
                    filePath = data.getData().getPath();
                    Toast.makeText(getContext(), filePath, Toast.LENGTH_SHORT).show();
//                    AudioDispatcher  dispatcher = AudioDispatcherFactory.fromPipe(filePath, 44100, 2048, 0);
//                  TarsosDSPAudioFormat tarsosDSPAudioFormat = new TarsosDSPAudioFormat(TarsosDSPAudioFormat.Encoding.PCM_SIGNED,
//                          16000,
//                          16,
//                          1,
//                          2,
//                          16000,
//                          ByteOrder.BIG_ENDIAN.equals(ByteOrder.nativeOrder()));
//
//                    PitchDetectionHandler phd = new PitchDetectionHandler() {
//
//                        public void handlePitch(final PitchDetectionResult res, AudioEvent e) {
//                            pitchInHz  = res.getPitch();
//                            if(pitchInHz != -1 && pitchInHz < 400)
//                                pitches.add(pitchInHz)
//                            });
//                        }
//
//                    AudioProcessor pitchProcessor = new PitchProcessor(new Yin(44100, 2048), 44100, 2048,);
//                        dispatcher.addAudioProcessor(pitchProcessor);
                }
                break;
            }
        }
    }
}


/*analyser le fichier audio :
- AudioDispatcherFactory.fromPipe() (selon steven) fromFile() sinon;

    TarsosDSPAudioFormat tarsosDSPAudioFormat = new TarsosDSPAudioFormat(TarsosDSPAudioFormat.Encoding.PCM_SIGNED,
            16000,
            16,
            1,
            2,
            16000,
            ByteOrder.BIG_ENDIAN.equals(ByteOrder.nativeOrder()));


PitchDetectionHandler pitchDetectionHandler = new PitchDetectionHandler() {

           public void handlePitch(final PitchDetectionResult res, AudioEvent e) {
               pitchInHz  = res.getPitch();
               //if(pitchInHz > 0){Log.d("EBB Outside Run","Pitch:" + pitchInHz);}
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                                if(pitchInHz > 0){Log.d("EBB Inside Run","Pitch:" + pitchInHz);}
                                pitchText.setText(pitchInHz + "");
                                processPitch(pitchInHz);
                    }
                });
            }

- pitchPorcessor --> Audioprocessor pitchprocessor = new Pitchprocessor

* */